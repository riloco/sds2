import React from 'react';
import "./styles.css";
import { ReactComponent as MainImage } from "./mainImage.svg";
import Footer from '../footer';
import { Link } from 'react-router-dom';

function Home() {
    return (
        <div>
            <div className="home-container">
                <div className="home-content">
                    <div className="home-actions">
                        <h1 className="home-title">
                            Haga su pedido <br /> que entregaremos <br /> para usted!!!
                        </h1>
                        <h3 className="home-subtitle">
                            Escoja su pedido que en pocos minutos <br /> llegaremos a su puerta
                        </h3>
                        <Link to="/orders" className="home-btn-order">
                            HACER PEDIDO
                        </Link>
                    </div>
                    <div className="home-image">
                        <MainImage />
                    </div>
                </div>
            </div>

            <Footer />
        </div>
    )
}

export default Home;