import React from "react";
import './styles.css';

function StepsHeaders() {
    return (
        <header className="orders-steps-container">
            <div className="orders-steps-content">
                <h1 className="steps-title">
                    SIGA LAS <br /> ETAPAS
                </h1>
                <ul className="steps-items">
                    <li>
                        <span className="steps-number">1</span>
                        Seleccione el producto y la localizacion
                    </li>

                    <li>
                        <span className="steps-number">2</span>
                        click en <strong>"ENVIAR PEDIDO"</strong>
                    </li>
                </ul>
            </div>

        </header>
    )
}


export default StepsHeaders