import axios from 'axios';

// const API_URL = 'http://localhost:8080';
const API_URL = 'http://192.168.20.25:8080';

export function fetchOrders() {
    console.log('url: ', `${API_URL}/orders`);
    
    return axios(`${API_URL}/orders`);
}

export function confirmDelivery(orderId: number){
    console.log('url: ', `${API_URL}/orders/${orderId}/delivered`);
    return axios.put(`${API_URL}/orders/${orderId}/delivered`);
}