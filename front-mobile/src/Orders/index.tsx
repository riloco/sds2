
import React, { useEffect, useState } from 'react';
import { useIsFocused, useNavigation } from '@react-navigation/native';
import { StyleSheet, Text, ScrollView, Image, Alert } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { fetchOrders } from '../api';
import Header from '../Header';
import OrdersCard from '../OrderCard';
import { Order } from '../types';

function Orders() {

    const [orders, serOrders] = useState<Order[]>([])
    const [isLoading, setIsLoading] = useState(false);
    const navigation = useNavigation();
    const isFocused = useIsFocused();

    const fetchData = () =>{
        setIsLoading(true);
        fetchOrders()
            .then(response => {
                console.log(response.data);
                serOrders(response.data)
            })
            .catch(error => Alert.alert('Error buscando los pedidos: ' + error))
            .finally(() => setIsLoading(false));
    }

    useEffect(() => {
        if (isFocused) {
            fetchData();
        }
    }, [isFocused])


    const handleOnPress = (order: Order) => {
        console.log('Detalle');
        
        navigation.navigate('OrderDetails', {order});
    }

    return (
        <>
            <Header />
            <ScrollView style={styles.container}>
                {isLoading ? (
                    <Text>Buscando Pedido</Text>
                ) : (

                        orders.map(order => (
                            <TouchableWithoutFeedback
                                key={order.id}
                                onPress={() => handleOnPress(order)}>

                                <OrdersCard order={order} />
                            </TouchableWithoutFeedback>
                        ))

                    )}

            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingRight: '5%',
        paddingLeft: '5%',
        backgroundColor: '#DDD'
    }
});

export default Orders