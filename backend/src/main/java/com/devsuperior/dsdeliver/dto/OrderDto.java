package com.devsuperior.dsdeliver.dto;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.devsuperior.dsdeliver.entities.Order;
import com.devsuperior.dsdeliver.entities.OrderStatus;

public class OrderDto {

    private Long id;
    private String address;
    private Double latitude;
    private Double longitude;
    private Instant moment;
    private OrderStatus status;
    private Double total;

    private List<ProductDto> products = new ArrayList<>();

    // public OrderDto() {
    // }

    public OrderDto(Long id, String address, Double latitude, Double longitude, Instant moment, OrderStatus status,
            Double total) {
        this.setId(id);
        this.setAddress(address);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setMoment(moment);
        this.setStatus(status);
        this.setTotal(total);
    }

    public OrderDto(Order entity) {
        setId(entity.getId());
        setAddress(entity.getAddress());
        setLatitude(entity.getLatitude());
        setLongitude(entity.getLongitude());
        setMoment(entity.getMoment());
        setStatus(entity.getStatus());
        setTotal(entity.getTotal());
        products = entity.getProducts().stream()
            .map(x -> new ProductDto(x)).collect(Collectors.toList());

    
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Instant getMoment() {
        return moment;
    }

    public void setMoment(Instant moment) {
        this.moment = moment;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    
}
