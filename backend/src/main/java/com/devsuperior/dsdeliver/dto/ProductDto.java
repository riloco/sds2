package com.devsuperior.dsdeliver.dto;

import java.io.Serializable;

import com.devsuperior.dsdeliver.entities.Product;

public class ProductDto implements Serializable{

    private static final long serialVersionUID = 1L;
    
    private Long id;
    private String name;
    private Double price;
    private String description;
    private String imageUri;

    public ProductDto(Long id, String name, Double price, String description, String imageUri) {
        super();
        this.setId(id);
        this.setName(name);
        this.setPrice(price);
        this.setDescription(description);
        this.setImageUri(imageUri);
    }



    public ProductDto(Product entity) {
        super();
        setId(entity.getId());
        setName(entity.getName());
        setPrice(entity.getPrice());
        setDescription(entity.getDescription());
        setImageUri(entity.getImageUri());
    }


    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

}
